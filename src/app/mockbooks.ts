import { Book } from './book';

export const BOOKS: Book[]=[
    {name:"Java8",author:"James Gosling",publisher:"",isbn:""},
    {name:"Java9",author:"Bagesh Sharma",publisher:"",isbn:""},
    {name:"Java10",author:"Shiva Mishra",publisher:"",isbn:""},
    {name:"Java11",author:"Deepak Mishra",publisher:"",isbn:""},
    {name:"Java12",author:"Amarish",publisher:"",isbn:""},
    {name:"Java13",author:"Ashutosh",publisher:"",isbn:""}];