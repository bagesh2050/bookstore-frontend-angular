export class Tokens {
  accesstoken: string;
  tokentype: string;
  refreshtoken: string;
  expiresin:number;
  scope:string;
}
