export class Book{
    name: String;
    isbn: String;
    author: String;
    publisher: String;
}