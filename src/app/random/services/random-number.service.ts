import { Injectable } from '@angular/core';
import { config } from 'src/app/config';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RandomNumberService {

  constructor(private http: HttpClient) {}

  //token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIHByaXZpbGFnZXMiOnsiYXBwbGljYXRpb25JZCI6IkJPT0tTVE9SRSIsInZlcnNpb24iOiJ2MSIsImFwaURldGFpbHMiOlt7ImFwaSI6IlVTRVIiLCJ2ZXJzaW9uIjoidjEiLCJyZXNwb25zZVN0cnVjdHVyZSI6W3sidmFyaWFibGUiOiJBTEwifV19XX0sInVzZXJfbmFtZSI6ImJvb2t1c2VyIiwic2NvcGUiOlsicmVhZCJdLCJleHAiOjE1OTM1NDc3ODgsImF1dGhvcml0aWVzIjpbIlJPTEVfVVNFUiJdLCJqdGkiOiI4MGExZDQ0NS1mNzFkLTQyMDAtYjI1ZS0wYzEwZDg0ZTA2YmQiLCJjbGllbnRfaWQiOiJCT09LU1RPUkVfVjEifQ.Gxyoor6_b_YKl6U_zoJjz1yiUrJBtmdlhjTt5bCBIzQMayylrU7gsSSoZz6ggd58ROz6J1It0pRhDdARSwvPu7Rr1t3K97ZdXiitHCoZu56qhCcLDHVDH0Yd50yNsKTjLkjbIrRFS96maRrjU19HfbTuMA5GPc61SiFm9SFEJfIiXWhOxgBEs6vzv5lOZ4uaNVfOFX59qbNscTzz4nUmXMD2fBkOUcps5cgf7X1rlWf_IRWGLGG67pHZ_bs_dZz6D6WnJQWlyCxAEssp0pwy41DgA7ftyF2lIxIxN1pJMOpm48a8f65_MsGyhmEh8ULfCZIwhWyMtY1vbThIzki9jQ";

  public getRandomNumber() {
    return this.http.get<any>(`${config.apiUrl}/book/author/James Goslin`,{ headers: this.getCustomHeaders()})
      .pipe(map(data => data.value));
  }

  getCustomHeaders(): HttpHeaders {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      //.set('Authorization', 'Bearer ' + this.token);
    return headers;
  }
}
