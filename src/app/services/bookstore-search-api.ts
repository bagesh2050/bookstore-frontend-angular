import { Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { config } from './../config';

@Injectable()
export class bookSearchAPIService
{

    constructor(private httpClient: HttpClient){ }

   getBooks(): Observable<any> {
        return this.httpClient.get(`${config.apiUrl}/inventory/book`,{ headers: this.getCustomHeaders()});
   }

   getCustomHeaders(): HttpHeaders {
     const headers = new HttpHeaders()
       .set('Content-Type', 'application/json')
       //.set('Authorization', 'Bearer ' + this.token);
     return headers;
   }
}