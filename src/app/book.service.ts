import { BOOKS } from './mockbooks';
import { Book } from './book';
import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  getBooks(): Observable<Book[]>{
    return of(BOOKS);
  }

  constructor() { }
}
