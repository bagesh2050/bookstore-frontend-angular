import { BookService } from './../book.service';
import { Book } from './../book';
import { BOOKS } from './../mockbooks';
import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { bookSearchAPIService } from './../services/bookstore-search-api';

@Component({
  selector: 'app-booklisting',
  templateUrl: './booklisting.component.html',
  styleUrls: ['./booklisting.component.css']
})
export class BooklistingComponent implements OnInit {

  books:Book[]; 
  selectedBook: Book;

  constructor(private bookService: bookSearchAPIService) { }

  ngOnInit(): void {
    this.bookService.getBooks().subscribe(books=>{this.books=books;});
  }

  onSelect(book: Book):void{
    this.selectedBook=book;
  }
}
