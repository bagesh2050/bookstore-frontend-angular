import { BookRoutingModule } from './book.routing.modules';
import { from } from 'rxjs';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { BooklistingComponent } from './booklisting.component'
import { BookDetailComponent } from '../book-detail/book-detail.component'
import { BookSearchComponent } from '../book-search/book-search.component';

@NgModule({
  declarations: [
    BooklistingComponent,
    BookDetailComponent,
    BookSearchComponent
  ],
  imports: [
    CommonModule,
    BookRoutingModule,
    MatButtonModule
  ]
})
export class BookModule { }
