import { bookSearchAPIService } from './services/bookstore-search-api';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import { BookDetailComponent } from './book-detail/book-detail.component'
import { HttpClientModule } from '@angular/common/http';
import {AuthModule } from './auth/auth.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
	  BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
	  AuthModule
  ],
  providers: [bookSearchAPIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
